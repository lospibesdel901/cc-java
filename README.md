# Informe de Trabajo final BBDD2


## Herramientas Utilizadas

-------------  
* PostgreSQL 
* Hibernate 
* Spring MVC

## Desarrollo

Nuestra aplicación se basa en un API REST que internamente almacene las cotizaciones de los productos en las diferentes tiendas más conocidas en el país, con el objeto de que el usuario pueda estar mejor informado acerca de los precios de los productos que desee comprar.

Para ello, en nuestro desarrollo empezamos a definir los modelos necesarios para la aplicación:
-------------
* **Sitio web** de la tienda
* **Producto** 
* **Categoría** del producto
* **Cotización** de un producto en una tienda

Luego de haber definido estos modelos como clases POJO’s, continuamos con la capa de persistencia utilizando el patrón Repository para acceder a los objetos persistidos como si fuera una colección de datos. En esta capa usamos Hibernate como ORM debido a que estábamos familiarizados con este framework y nos resultaba sencillo el mapeo de nuestros modelos a través de XML, como también la forma de escribir las Queries para acceder a los datos. 

Como último paso implementamos las operaciones CRUD y consultas necesarias para la aplicación, como por ejemplo la búsqueda de la cotización más baja para un producto en particular. Estos métodos los implementamos en una capa de Servicio, y los expusimos a través de una API REST con Spring MVC. 

Al haber implementado estas últimas capas nos percatamos que en algunos niveles invocábamos métodos explícitos de Hibernate para el manejo de los objetos de la aplicación, lo que nos hacía depender fuertemente de esta herramienta. Por ejemplo, si quisieramos cambiar de framework tendríamos que modificar nuestro código en múltiples  niveles de aplicación.

Para resolver este problema de diseño, empezamos a modificar el mapeo y la forma en la que se accedía a los datos teniendo en mente la Persistencia por alcance, utilizando un objeto principal al que llamamos “Main”. A partir de este objeto, se puede acceder a todos los objetos persistidos de la aplicación sin ningún método explícito de Hibernate de por medio, eliminando los repositorios de los modelos de la aplicación como método de acceso.

Como última modificación, agregamos la anotación @Transactional que nos provee Spring, que nos sirve de Decorator para evitar abrir y cerrar explícitamente cada transacción en nuestra capa de Servicios. También nos liberó de código repetitivo en estos métodos.







## API

**Show all categories**
----
* **URL**
    /categories
* **Method:**
    `GET`
 
* **URL Params**
    None
* **Data Params**
    None
* **Success Response:**
  * **Code:** 200 
    **Content:** `{
      "result": "OK",
      "resultingObject": [
          {
              "id"   : 1,
              "name" : "cat_1"
          }
      ]
  }`

**Create category**
----
* **URL**
    /categories/
* **Method:**
    `POST`
* **URL Params**
    None
* **Data Params**
    `{name : 'new_category'}`
* **Success Response:**
  * **Code:** 200 
    **Content:** `{"result": "OK"}`


**Update category**
----
* **URL**
    /categories/:id
* **Method:**
    `PUT`
* **URL Params**
    Required: id=[integer]
* **Data Params**
    `{name: 'update_name_category'}`
* **Success Response:**
  * **Code:** 200 
    **Content:** `{"result": "OK"}`

**Delete category**
----
* **URL**
    /categories/:id
* **Method:**
    `DELETE`
* **URL Params**
    Required: id=[integer]
* **Data Params**
    None
* **Success Response:**
  * **Code:** 200 
    **Content:** `{"result": "OK"}`













**Show all products**
----
* **URL**
    /products
* **Method:**
    `GET`
* **URL Params**
    None
* **Data Params**
    None
* **Success Response:**
  * **Code:** 200 
    **Content:** `{
      "result": "OK",
      "resultingObject": [
          {
            "category": 1,
            "categoryDTO": {
                "id": 1,
                "name": "cat_1"
            },
            "id": 1,
            "name": "name_product"
        }
      ]
  }`

**Create product**
----
* **URL**
    /products/
* **Method:**
    `POST`
* **URL Params**
    None
* **Data Params**
    `
    {
     name     : 'prod_1', 
     category : '1'
    }`
* **Success Response:**
  * **Code:** 200 
    **Content:** `{"result": "OK"}`

**Update product**
----
* **URL**
    /products/:id
* **Method:**
    `PUT`
* **URL Params**
    Required: id=[integer]
* **Data Params**
    `
    {
     name     : 'prod_1', 
     category : '1'
    }`
* **Success Response:**
  * **Code:** 200 
    **Content:** `{"result": "OK"}`

**Delete product**
----
* **URL**
    /products/:id
* **Method:**
    `DELETE`
* **URL Params**
    Required: id=[integer]
* **Data Params**
    None
* **Success Response:**
  * **Code:** 200 
    **Content:** `{"result": "OK"}`















**Show all sites**
----
* **URL**
    /sites
* **Method:**
    `GET`
* **URL Params**
    None
* **Data Params**
    None
* **Success Response:**
  * **Code:** 200 
    **Content:** `{
      "result": "OK",
      "resultingObject": [
          {
            "id"   : 1,
            "name" : "site 1",
            "url"  : "www.site1.com"
        }
      ]
  }`


**Create site**
----
* **URL**
    /sites/
* **Method:**
    `POST`
* **URL Params**
    None
* **Data Params**
    `
    {
     name : 'name_site', 
     url  : 'www.site.com'
    }`
* **Success Response:**
  * **Code:** 200 
    **Content:** `{"result": "OK"}`

**Update site**
----
* **URL**
    /sites/:id
* **Method:**
    `PUT`
* **URL Params**
    Required: id=[integer]
* **Data Params**
    `
    {
     name : 'name_site', 
     url  : 'www.site.com'
    }`
* **Success Response:**
  * **Code:** 200 
    **Content:** `{"result": "OK"}`


**Delete site**
----
* **URL**
    /sites/:id
* **Method:**
    `DELETE`
* **URL Params**
    Required: id=[integer]
* **Data Params**
    None
* **Success Response:**
  * **Code:** 200 
    **Content:** `{"result": "OK"}`






**Create quotation**
----
* **URL**
    /quotations/
* **Method:**
    `POST`
* **URL Params**
    None
* **Data Params**
    `
    {
      product : 1,
      site    : 1,
      price   : 1.3,
      ts      : '20171212'
    }`
* **Success Response:**
  * **Code:** 200 
    **Content:** `{"result": "OK"}`



**Update quotation**
----
* **URL**
    /quotations/:id
* **Method:**
    `PUT`
* **URL Params**
    Required: id=[integer]
* **Data Params**
    `
    {
      product : 1,
      site    : 2,
      price   : 2.3,
      ts      : '20171213'
    }`
* **Success Response:**
  * **Code:** 200 
    **Content:** `{"result": "OK"}`


**Delete site**
----
* **URL**
    /quotations/:id
* **Method:**
    `DELETE`
* **URL Params**
    Required: id=[integer]
* **Data Params**
    None
* **Success Response:**
  * **Code:** 200 
    **Content:** `{"result": "OK"}`





**Show min quotation of all products**
----
* **URL**
    /quotations/min
* **Method:**
    `GET`
* **URL Params**
    None
* **Data Params**
    None
* **Success Response:**
  * **Code:** 200 
    **Content:** `{
      "result": "OK",
      "resultingObject": [
        {
            "id": 2,
            "price": 3.3,
            "productDTO": {
                "category": 1,
                "categoryDTO": {
                    "id": 1,
                    "name": "cat_1"
                },
                "id": 1,
                "name": "prod_1_updated"
            },
            "siteDTO": {
                "id": 1,
                "name": "site 1",
                "url": "www.gg.com"
            },
            "ts": "Dec 11, 2017 12:00:00 AM"
        }
      ]
  }`