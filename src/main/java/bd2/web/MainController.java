package bd2.web;


import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.google.gson.Gson;

import bd2.bd2.dto.CategoryDTO;
import bd2.bd2.dto.ProductDTO;
import bd2.bd2.dto.QuotationDTO;
import bd2.bd2.dto.SiteDTO;
import bd2.bd2.service.ServiceLocator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


@Controller
@ResponseBody
@EnableWebMvc
public class MainController {
	@Autowired
	ServiceLocator serviceLocator;
	
	public ServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	public void setServiceLocator(ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}
	
	@RequestMapping(value = "/sites", method = RequestMethod.GET, produces = "application/json", headers = "Accept=application/json")
	public String sites() {
		Map<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("result", "OK");
		Set<SiteDTO> siteDTO = (Set<SiteDTO>)serviceLocator.getSiteService().retrieveAll();
		aMap.put("resultingObject", siteDTO);
		return new Gson().toJson(aMap);
	}
	
	@RequestMapping(value = "/products", method = RequestMethod.GET, produces = "application/json", headers = "Accept=application/json")
	public String products() {
		Map<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("result", "OK");
		Set<ProductDTO> productsDTO = (Set<ProductDTO>)serviceLocator.getProductService().retrieveAll();
		aMap.put("resultingObject", productsDTO);
		return new Gson().toJson(aMap);
	}
	
	@RequestMapping(value = "/categories", method = RequestMethod.GET, produces = "application/json", headers = "Accept=application/json")
	public String categories() {
		Map<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("result", "OK");
		Set<CategoryDTO> categoriesDTO = (Set<CategoryDTO>)serviceLocator.getCategoryService().retrieveAll();
		aMap.put("resultingObject", categoriesDTO);
		return new Gson().toJson(aMap);
	}
	
	
	@RequestMapping(value = "/quotations", method = RequestMethod.GET, produces = "application/json", headers = "Accept=application/json")
	public String quotations() {
		Map<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("result", "OK");
		Set<QuotationDTO> quotationsDTO = (Set<QuotationDTO>)serviceLocator.getQuotationService().retrieveAll();
		aMap.put("resultingObject", quotationsDTO);
		return new Gson().toJson(aMap);
	}
	
	
	
	@RequestMapping(value="/sites", method=RequestMethod.POST)
	public ResponseEntity<Void> addSite(@RequestBody SiteDTO siteDTO) {
		if(serviceLocator.getSiteService().insert(siteDTO)){
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@RequestMapping(value="/categories", method=RequestMethod.POST)
	public ResponseEntity<Void> addCategory(@RequestBody  CategoryDTO categoryDTO) {
		
		if(serviceLocator.getCategoryService().insert(categoryDTO)){
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	
	@RequestMapping(value="/products", method=RequestMethod.POST)
	public ResponseEntity<Void> addProduct(@RequestBody ProductDTO productDTO) {
		if(serviceLocator.getProductService().insert(productDTO)){
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@RequestMapping(value="/quotations", method=RequestMethod.POST)
	public ResponseEntity<Void> addQuotation(@RequestBody QuotationDTO quotationDTO) { 
		if(serviceLocator.getQuotationService().insert(quotationDTO)){
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@RequestMapping(value="/sites/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> updateSite(@PathVariable("id") long id, @RequestBody SiteDTO siteDTO) { 
		if(serviceLocator.getSiteService().update(id, siteDTO)){
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@RequestMapping(value="/categories/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> updateCategory(@PathVariable("id") long id, @RequestBody CategoryDTO categoryDTO) { 
		if(serviceLocator.getCategoryService().update(id, categoryDTO)){
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@RequestMapping(value="/products/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> updateProduct(@PathVariable("id") long id, @RequestBody ProductDTO productDTO) {
				
		if(serviceLocator.getProductService().update(id, productDTO)){
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@RequestMapping(value="/quotations/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> updateQuotation(@PathVariable("id") long id, @RequestBody QuotationDTO quotationDTO) {
				
		if(serviceLocator.getQuotationService().update(id, quotationDTO)){
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@RequestMapping(value="/sites/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> deleteSite(@PathVariable("id") long id) { 
		if(serviceLocator.getSiteService().delete(id)){
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	@RequestMapping(value="/categories/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> deleteCategory(@PathVariable("id") long id) { 
		if(serviceLocator.getCategoryService().delete(id)){
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	@RequestMapping(value="/products/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> deleteProduct(@PathVariable("id") long id) {
		if(serviceLocator.getProductService().delete(id)){
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@RequestMapping(value="/quotations/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> deleteQuotation(@PathVariable("id") long id) {	
		if(serviceLocator.getQuotationService().delete(id)){
			return new ResponseEntity<Void>(HttpStatus.OK);
		} else return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@RequestMapping(value = "/quotations/min", method = RequestMethod.GET, produces = "application/json", headers = "Accept=application/json")
	public String minQuotations() {
		Map<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("result", "OK");
		Set<QuotationDTO> quotationDTOs = (Set<QuotationDTO>)serviceLocator.getQuotationService().getProductsMinQuotation();
		aMap.put("resultingObject", quotationDTOs);
		return new Gson().toJson(aMap);
	}
	
	@RequestMapping(value = "/quotations/min/{id}", method = RequestMethod.GET, produces = "application/json", headers = "Accept=application/json")
	public String minQuotationProduct(@PathVariable("id") long id) {
		Map<String, Object> aMap = new HashMap<String, Object>();
		aMap.put("result", "OK");
		QuotationDTO quotationDTO = (QuotationDTO)serviceLocator.getQuotationService().getMinQuotationProduct(id);
		aMap.put("resultingObject", quotationDTO);
		return new Gson().toJson(aMap);
	}


}
