package bd2.bd2;


import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import bd2.bd2.model.Category;
import bd2.bd2.model.MainModel;
import bd2.bd2.model.Product;
import bd2.bd2.model.Quotation;
import bd2.bd2.model.Site;
import bd2.bd2.repository.MainRepository;


public class Importer {
    
    static MainRepository mainRepository = null;
    private static AbstractApplicationContext abstractApplicationContext;
    static MainModel main = null;
    
    public static void main(String[] args) {
        initialize();
        populate();        
    }
    
    private static void initialize() {
        
        abstractApplicationContext = new ClassPathXmlApplicationContext("beans.xml");
        mainRepository = (MainRepository) abstractApplicationContext.getBean("MainRepository");
        main = mainRepository.getMain();


    }
    @Transactional
    public static void populate(){
                
        // Create sites
        Site site = new Site();
        site.setName("Garbarino");
        site.setUrl("www.garbarino.com");
        main.addSite(site);
        
        
        Site site2 = new Site();
        site2.setName("Fravega");
        site2.setUrl("www.fravega.com");
        main.addSite(site2);
        
        
        Site site3 = new Site();
        site3.setName("Compumondo");
        site3.setUrl("www.compumondo.com");
        main.addSite(site3); 
        

        //Create categories    
        Category category = new Category();
        category.setName("Electrodoméstico");
        main.addCategory(category);
        
        
        Category category2 = new Category();
        category2.setName("Informática");
        main.addCategory(category2);


        Category category3 = new Category();
        category3.setName("Calefacción");
        main.addCategory(category3);




        //create products
        Product product = new Product();
        product.setCategory(category);
        product.setName("Heladera Mabe");
            main.addProduct(product);


        Product product2 = new Product();
        product2.setCategory(category);
        product2.setName("Microondas ATMA");
            main.addProduct(product2);


        Product product3 = new Product();
        product3.setCategory(category2);
        product3.setName("Teclado HyperX");
            main.addProduct(product3);


        Product product4 = new Product();
        product4.setCategory(category2);
        product4.setName("Mouse Logitech");
            main.addProduct(product4);


        Product product5 = new Product();
        product5.setCategory(category3);
        product5.setName("Aire Acondicionado SIGNAL");
            main.addProduct(product5);


        Product product6 = new Product();
        product6.setCategory(category3);
        product6.setName("Estufa Liliana");
            main.addProduct(product6);

        
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            
            Quotation quotation = new Quotation();
            quotation.setProduct(product);
            quotation.setTs( sdf.parse("10/10/2017") );
            quotation.setPrice(15299.00f);
            quotation.setSite(site);
            main.addQuotation(quotation);
            
            
            Quotation quotation2 = new Quotation();
            quotation2.setProduct(product);
            quotation2.setTs( sdf.parse("10/10/2017") );
            quotation2.setPrice(14299.00f);
            quotation2.setSite(site2);
            main.addQuotation(quotation2);
            
            
            Quotation quotation3 = new Quotation();
            quotation3.setProduct(product);
            quotation3.setTs( sdf.parse("10/10/2017") );
            quotation3.setPrice(15599.00f);
            quotation3.setSite(site3);
            main.addQuotation(quotation3);





            Quotation quotation4 = new Quotation();
            quotation4.setProduct(product2);
            quotation4.setTs( sdf.parse("10/10/2017") );
            quotation4.setPrice(3800.00f);
            quotation4.setSite(site);
            main.addQuotation(quotation4);
            
            
            Quotation quotation5 = new Quotation();
            quotation5.setProduct(product2);
            quotation5.setTs( sdf.parse("10/10/2017") );
            quotation5.setPrice(3500.00f);
            quotation5.setSite(site2);
            main.addQuotation(quotation5);
            
            
            Quotation quotation6 = new Quotation();
            quotation6.setProduct(product2);
            quotation6.setTs( sdf.parse("10/10/2017") );
            quotation6.setPrice(4100.00f);
            quotation6.setSite(site3);
            main.addQuotation(quotation6);







            Quotation quotation7 = new Quotation();
            quotation7.setProduct(product3);
            quotation7.setTs( sdf.parse("10/10/2017") );
            quotation7.setPrice(1500.00f);
            quotation7.setSite(site);
            main.addQuotation(quotation7);
            
            
            Quotation quotation8 = new Quotation();
            quotation8.setProduct(product3);
            quotation8.setTs( sdf.parse("10/10/2017") );
            quotation8.setPrice(1700.00f);
            quotation8.setSite(site2);
            main.addQuotation(quotation8);
            
            
            Quotation quotation9 = new Quotation();
            quotation9.setProduct(product3);
            quotation9.setTs( sdf.parse("10/10/2017") );
            quotation9.setPrice(2100.00f);
            quotation9.setSite(site3);
            main.addQuotation(quotation9);






            Quotation quotation10 = new Quotation();
            quotation10.setProduct(product4);
            quotation10.setTs( sdf.parse("10/10/2017") );
            quotation10.setPrice(1999.99f);
            quotation10.setSite(site);
            main.addQuotation(quotation10);
            
            
            Quotation quotation11 = new Quotation();
            quotation11.setProduct(product4);
            quotation11.setTs( sdf.parse("10/10/2017") );
            quotation11.setPrice(1645.00f);
            quotation11.setSite(site2);
            main.addQuotation(quotation11);
            
            
            Quotation quotation12 = new Quotation();
            quotation12.setProduct(product4);
            quotation12.setTs( sdf.parse("10/10/2017") );
            quotation12.setPrice(2100.00f);
            quotation12.setSite(site3);
            main.addQuotation(quotation12);






            Quotation quotation13 = new Quotation();
            quotation13.setProduct(product5);
            quotation13.setTs( sdf.parse("10/10/2017") );
            quotation13.setPrice(16999.99f);
            quotation13.setSite(site);
            main.addQuotation(quotation13);
            
            
            Quotation quotation14 = new Quotation();
            quotation14.setProduct(product5);
            quotation14.setTs( sdf.parse("10/10/2017") );
            quotation14.setPrice(15999.00f);
            quotation14.setSite(site2);
            main.addQuotation(quotation14);
            
            
            Quotation quotation15 = new Quotation();
            quotation15.setProduct(product5);
            quotation15.setTs( sdf.parse("10/10/2017") );
            quotation15.setPrice(17999.00f);
            quotation15.setSite(site3);
            main.addQuotation(quotation15);







            Quotation quotation16 = new Quotation();
            quotation16.setProduct(product6);
            quotation16.setTs( sdf.parse("10/10/2017") );
            quotation16.setPrice(3700.99f);
            quotation16.setSite(site);
            main.addQuotation(quotation16);
            
            
            Quotation quotation17 = new Quotation();
            quotation17.setProduct(product6);
            quotation17.setTs( sdf.parse("10/10/2017") );
            quotation17.setPrice(3900.00f);
            quotation17.setSite(site2);
            main.addQuotation(quotation17);
            
            
            Quotation quotation18 = new Quotation();
            quotation18.setProduct(product6);
            quotation18.setTs( sdf.parse("10/10/2017") );
            quotation18.setPrice(3500.00f);
            quotation18.setSite(site3);
            main.addQuotation(quotation18);
            
            
            
            
            
            
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        
        
        
    }
    

    public static void print(String s) {
        System.out.println(s);
    }
}
