package bd2.bd2.dto;
import java.util.Date;
import bd2.bd2.model.Quotation;

public class QuotationDTO {
	
	private Long id;
	private Date ts;
	private Float price;
	private ProductDTO productDTO;
	private SiteDTO siteDTO;
	private Long site;
	private Long product;

	public QuotationDTO (Quotation quotation){
		
		this.id = quotation.getId();
		this.ts = quotation.getTs();
		this.price = quotation.getPrice();
		this.productDTO = new ProductDTO(quotation.getProduct());
		this.siteDTO = new SiteDTO(quotation.getSite());
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Date getTs() {
		return ts;
	}
	public void setTs(Date ts) {
		this.ts = ts;
	}
	
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}

	public SiteDTO getSiteDTO() {
		return siteDTO;
	}
	public void setSiteDTO(SiteDTO site) {
		this.siteDTO = site;
	}
	
	public void setSite(Long site) {
		this.site = site;
	}

	public void setProduct(Long product) {
		this.product = product;
	}
	
	public Long getSite() {
		return site;
	}

	public Long getProduct() {
		return product;
	}

	
	public ProductDTO getProductDTO() {
		return productDTO;
	}

	public void setProductDTO(ProductDTO productDTO) {
		this.productDTO = productDTO;
	}

	public String toString(){
		return id + " price: " + price +
		"\nts: " + ts.toString() + 
		"\nproduct: " + productDTO.toString() + 
		"\nsite: " + siteDTO.toString();
		
	}
	
	

}
