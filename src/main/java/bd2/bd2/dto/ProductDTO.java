package bd2.bd2.dto;

import bd2.bd2.model.Product;

public class ProductDTO {
	
	
	private Long id;
	private String name;
	private CategoryDTO categoryDTO;
	private Long category;
	
	
	public ProductDTO (Product product) {
		
		this.id = product.getId();
		this.name = product.getName();
		this.categoryDTO = new CategoryDTO(product.getCategory());
		this.category = product.getCategory().getId();
		
	}
	
	

	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}
	
	
	public Long getCategory() {
		return category;
	}


	public void setCategory(Long id) {
		this.category = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public CategoryDTO getCategoryDTO() {
		return categoryDTO;
	}


	public void setCategoryDTO(CategoryDTO categoryDTO) {
		this.categoryDTO = categoryDTO;
	}
	
	
	public String toString(){
		return id + ": " + name + " Cat: " + category;
		// return id + ": " + name;
	}
}