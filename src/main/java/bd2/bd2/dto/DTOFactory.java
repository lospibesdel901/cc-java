package bd2.bd2.dto;

import bd2.bd2.model.Product;
import bd2.bd2.model.Site;
import bd2.bd2.model.Quotation;
import bd2.bd2.model.Category;

public class DTOFactory {
	
	
	private static DTOFactory dtoFactory;
	
	public static DTOFactory getInstance() {
		// TODO Auto-generated method stub
		
		if (dtoFactory == null) {
			dtoFactory  = new DTOFactory(); 
		}

		return dtoFactory;
	}
	
	public ProductDTO createProductDTO(Product product) {

		return new ProductDTO( product );
		
	}
	
	public SiteDTO createSiteDTO(Site site) {

		return new SiteDTO(site);
		
	}
	
	public QuotationDTO createQuotationDTO(Quotation quotation) {
		
		return new QuotationDTO(quotation);
				
	}
	
	public CategoryDTO createCategoryDTO(Category category) {
		
		return new CategoryDTO(category);
				
	}
	

}
