package bd2.bd2.repository;

import org.hibernate.Query;
import org.hibernate.Session;

import bd2.bd2.model.MainModel;

public class MainRepository extends Repository{
	
	public MainModel getMain(){		
		Session session = sessionFactory.getCurrentSession();
		System.out.println("session:" +  session.toString());		
		Query query = session.createQuery("FROM MainModel m ORDER BY id desc");
		MainModel mainModel= (MainModel) query.list().get(0);
		return mainModel;
	}

}
