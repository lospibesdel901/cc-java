package bd2.bd2.repository;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;


public class Repository {
	
	@Autowired
	protected SessionFactory sessionFactory = null;
	
	public void setSessionFactory(SessionFactory sFactory) {
		this.sessionFactory = sFactory;
	}
	
}
