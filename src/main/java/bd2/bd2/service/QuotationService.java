package bd2.bd2.service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


import org.springframework.transaction.annotation.Transactional;


import bd2.bd2.dto.DTOFactory;
import bd2.bd2.dto.ProductDTO;
import bd2.bd2.dto.QuotationDTO;
import bd2.bd2.dto.SiteDTO;
import bd2.bd2.model.MainModel;
import bd2.bd2.model.Product;
import bd2.bd2.model.Quotation;
import bd2.bd2.model.Site;

public class QuotationService  extends Service{
	
	@Transactional
	public Set<QuotationDTO> retrieveAll(){
		
		MainModel main = getMainRepository().getMain();
		Iterator<Quotation> iterator = main.getQuotations().iterator();
		Set<QuotationDTO> quotationDTO = new HashSet<QuotationDTO>();
		while(iterator.hasNext()){
			quotationDTO.add( DTOFactory.getInstance().createQuotationDTO(iterator.next()) );
		}
		return quotationDTO;
	}
	
	@Transactional
	public boolean insert(QuotationDTO quotationDTO){
		
		MainModel main = getMainRepository().getMain();		
		Product product = main.getProductByID(quotationDTO.getProduct());
		Site site = main.getSiteByID(quotationDTO.getSite());
		quotationDTO.setSiteDTO(new SiteDTO(site));
		quotationDTO.setProductDTO(new ProductDTO(product));
		
		Quotation model = new Quotation(quotationDTO);
		model.setSite(site);
		model.setProduct(product);
		main.addQuotation(model);
		return true;		
	}
	
	@Transactional
	public boolean update(long id, QuotationDTO quotationDTO) {
		
		
		MainModel main = getMainRepository().getMain();		
		Product product = main.getProductByID(quotationDTO.getProduct());
		Site site = main.getSiteByID(quotationDTO.getSite());
		
		
		quotationDTO.setSiteDTO(new SiteDTO(site));
		quotationDTO.setProductDTO(new ProductDTO(product));
		Quotation model = main.getQuotationByID(id);
		model.setSite(site);
		model.setProduct(product);
		model.update(quotationDTO);
		return true;
	}
	
	@Transactional
	public boolean delete(long id) {
		MainModel main = getMainRepository().getMain();
		Quotation quotation = main.getQuotationByID(id);
		if ( quotation != null ) {
			main.removeQuotation(quotation);
		}
		return true;		
	}
	
	@Transactional
	public Set<QuotationDTO> getProductsMinQuotation(){
		MainModel main = getMainRepository().getMain();
//		Session session = this.getSessionFactory().openSession();
		List<Quotation> quotations = main.retrieveProductsMinQuotation();
		Iterator<Quotation> iterator = quotations.iterator();
		Set<QuotationDTO> quotationDTO = new HashSet<QuotationDTO>();
		while(iterator.hasNext()){
			quotationDTO.add( DTOFactory.getInstance().createQuotationDTO(iterator.next()) );
		}
		return quotationDTO;
	}
	
	@Transactional
	public QuotationDTO getMinQuotationProduct(Long id){
		//Session session = this.getSessionFactory().openSession();
		MainModel main = getMainRepository().getMain();
		Quotation quotation = main.retrieveMinQuotationProduct(id);
		QuotationDTO quotationDTO = null;
		if (quotation != null ){
			quotationDTO =  DTOFactory.getInstance().createQuotationDTO(quotation);
		}
		return quotationDTO;		
	}

}
