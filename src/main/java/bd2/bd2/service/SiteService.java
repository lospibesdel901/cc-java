package bd2.bd2.service;

import java.util.HashSet;
import java.util.Iterator;

import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import bd2.bd2.dto.DTOFactory;

import bd2.bd2.dto.SiteDTO;

import bd2.bd2.model.MainModel;

import bd2.bd2.model.Site;

public class SiteService extends Service {
	@Transactional
	public Set<SiteDTO> retrieveAll() {

		MainModel main = getMainRepository().getMain();
		Iterator<Site> iterator = main.getSites().iterator();
		Set<SiteDTO> siteDTO = new HashSet<SiteDTO>();
		while (iterator.hasNext()) {
			siteDTO.add(DTOFactory.getInstance().createSiteDTO(iterator.next()));
		}
		return siteDTO;
	}

	@Transactional
	public boolean insert(SiteDTO siteDTO) {
		Site site = new Site(siteDTO);
		MainModel main = getMainRepository().getMain();
		main.addSite(site);
		return true;
	}

	@Transactional
	public boolean update(long id, SiteDTO siteDTO) {
		MainModel main = getMainRepository().getMain();
		Site site = main.getSiteByID(id);
		site.update(siteDTO);
		return true;
	}

	@Transactional
	public boolean delete(long id) {
		MainModel main = getMainRepository().getMain();
		Site site = main.getSiteByID(id);
		if (site != null) {
			main.removeSite(site);
		}
		return true;
	}

}
