package bd2.bd2.service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.springframework.transaction.annotation.Transactional;

import bd2.bd2.dto.CategoryDTO;
import bd2.bd2.dto.DTOFactory;
import bd2.bd2.dto.ProductDTO;
import bd2.bd2.model.Category;
import bd2.bd2.model.MainModel;
import bd2.bd2.model.Product;

public class ProductService extends Service {
	
	@Transactional
	public Set<ProductDTO> retrieveAll(){
		
		MainModel main = getMainRepository().getMain();
		Iterator<Product> iterator = main.getProducts().iterator();		
		Set<ProductDTO> productsDTO = new HashSet<ProductDTO>();
		while(iterator.hasNext()){
			productsDTO.add( DTOFactory.getInstance().createProductDTO(iterator.next()) );
		}
		return productsDTO;
	}
	
	@Transactional
	public boolean insert(ProductDTO productDTO){
		
		
		MainModel main = getMainRepository().getMain();
		Category category = main.getCategoryByID(productDTO.getCategory());
		productDTO.setCategoryDTO(new CategoryDTO(category));
		Product model = new Product(productDTO);
		model.setCategory(category);		
		main.addProduct(model);
		return true;
	}
	
	@Transactional
	public boolean update(long id, ProductDTO productDTO) {
		MainModel main = getMainRepository().getMain();
		Category category = main.getCategoryByID(productDTO.getCategory());
		productDTO.setCategoryDTO(new CategoryDTO(category));
		Product model = main.getProductByID(id);
		model.setCategory(category);
		model.update(productDTO);
		return true;	
	}
	
	@Transactional
	public boolean delete(long id) {
		MainModel main = getMainRepository().getMain();
		Product product = main.getProductByID(id);
		if ( product != null ) {
			main.removeProduct(product);
		}
		return true;
	}
}
