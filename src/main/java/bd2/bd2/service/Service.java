package bd2.bd2.service;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import bd2.bd2.repository.MainRepository;


@Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = {Exception.class})
public class Service {
	
	protected MainRepository mainRepository;	
	
	public MainRepository getMainRepository() {
		return mainRepository;
	}

	public void setMainRepository(MainRepository mainRepository) {
		this.mainRepository = mainRepository;
	}
}
