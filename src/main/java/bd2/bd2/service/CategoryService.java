package bd2.bd2.service;

import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import bd2.bd2.dto.CategoryDTO;
import bd2.bd2.dto.DTOFactory;
import bd2.bd2.model.Category;
import bd2.bd2.model.MainModel;
//import bd2.bd2.model.Site;

public class CategoryService extends Service {
	
	
	@Transactional
	public Set<CategoryDTO> retrieveAll() {
		MainModel main = getMainRepository().getMain();
		Iterator<Category> iterator = main.getCategories().iterator();
		Set<CategoryDTO> categoryDTO = new HashSet<CategoryDTO>();
		while (iterator.hasNext()) {
			categoryDTO.add(DTOFactory.getInstance().createCategoryDTO(iterator.next()));
		}
		return categoryDTO;
	}
	
	@Transactional
	public boolean insert(CategoryDTO categoryDTO) {
		Category model = new Category(categoryDTO);
		MainModel main = getMainRepository().getMain();
		main.addCategory(model);
		return true;
	}
	
	@Transactional
	public boolean update(long id, CategoryDTO categoryDTO) {
		//Category category = getCategoryRepository().get(id);
		MainModel main = getMainRepository().getMain();
		Category category = main.getCategoryByID(id);
		category.update(categoryDTO);
		return true;		
	}
	
	@Transactional
	public boolean delete(long id) {
		MainModel main = getMainRepository().getMain();
		Category category = main.getCategoryByID(id);
		if ( category != null ) {
			main.removeCategory(category);
		}
		return true;
	}
}
