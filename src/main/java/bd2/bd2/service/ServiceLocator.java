package bd2.bd2.service;

public class ServiceLocator {
		
	private static ServiceLocator serviceLocator;
	
	private SiteService siteService;
	private ProductService productService;
	private QuotationService quotationService;
	private CategoryService categoryService;
	
	public static ServiceLocator getInstance(){
		if(serviceLocator == null){
			serviceLocator = new ServiceLocator();
		}
		return serviceLocator;
	}
	
	public static ServiceLocator getServiceLocator() {
		return serviceLocator;
	}
	public static void setServiceLocator(ServiceLocator serviceLocator) {
		ServiceLocator.serviceLocator = serviceLocator;
	}
		
	public CategoryService getCategoryService() {
		return categoryService;
	}

	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}

	public SiteService getSiteService() {
		return siteService;
	}

	public void setSiteService(SiteService siteService) {
		this.siteService = siteService;
	}

	public ProductService getProductService() {
		return productService;
	}

	public void setProductService(ProductService productService) {
		this.productService = productService;
	}

	public QuotationService getQuotationService() {
		return quotationService;
	}

	public void setQuotationService(QuotationService quotationService) {
		this.quotationService = quotationService;
	}
}