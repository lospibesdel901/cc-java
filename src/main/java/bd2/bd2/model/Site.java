package bd2.bd2.model;

import bd2.bd2.dto.SiteDTO;

public class Site {
	
	public Site(){
		
	}
	public Site(SiteDTO siteDTO){
		name = siteDTO.getName();
		url = siteDTO.getUrl();
	}
	
	public void update(SiteDTO siteDTO) {
		name = siteDTO.getName();
		url = siteDTO.getUrl();
	}
	
	private Long id;
	private String name;
	private String url;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}

}
