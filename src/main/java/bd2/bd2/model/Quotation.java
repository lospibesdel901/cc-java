package bd2.bd2.model;
import java.util.Date;

import bd2.bd2.dto.QuotationDTO;


public class Quotation {
	
	private Long id;
	private Date ts;
	private Float price;
	private Product product;
	private Site site;
	
	public Quotation(){
		super();
	}
	
	
	public Quotation(QuotationDTO quotationDTO){
		ts = quotationDTO.getTs();
		price = quotationDTO.getPrice();
		product = new Product(quotationDTO.getProductDTO());
		site = new Site(quotationDTO.getSiteDTO());
	}
	
	public void update(QuotationDTO quotationDTO) {
		ts = quotationDTO.getTs();
		price = quotationDTO.getPrice();
		//product = new Product(quotationDTO.getProductDTO());
		//site = new Site(quotationDTO.getSiteDTO());
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public Date getTs() {
		return ts;
	}
	public void setTs(Date ts) {
		this.ts = ts;
	}
	
	public Float getPrice() {
		return price;
	}
	public void setPrice(Float price) {
		this.price = price;
	}
	
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	
	public Site getSite() {
		return site;
	}
	public void setSite(Site site) {
		this.site = site;
	}
	
	public String toString(){
		return "(" + product.getId() +  ")" + product.getName() + " $" + getPrice() + " on " + site.getUrl();  
	}

	
}
