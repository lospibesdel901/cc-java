package bd2.bd2.model;

import bd2.bd2.dto.ProductDTO;

public class Product {
	
	
	public Product (){
	
	}
	
	public Product(ProductDTO productDTO){
		name = productDTO.getName();
		category = new Category(productDTO.getCategoryDTO());
	}
	
	public void update(ProductDTO productDTO) {
		name = productDTO.getName();
		//category = new Category(productDTO.getCategoryDTO());
		
	}
	
	private Long id;
	private String name;
	private Category category;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	
	public String toString(){
		return id + ": " + name + " Cat: " + category.toString();
	}


}
