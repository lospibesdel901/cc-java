package bd2.bd2.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;

public class MainModel implements Serializable {

	private int mid;
	Set<Site> sites = new HashSet<Site>();
	Set<Quotation> quotations = new HashSet<Quotation>();
	Set<Category> categories = new HashSet<Category>();
	Set<Product> products = new HashSet<Product>();

	public int getMid() {
		return mid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public Set<Site> getSites() {
		return sites;
	}

	public void setSites(Set<Site> sites) {
		this.sites = sites;
	}

	public Set<Quotation> getQuotations() {
		return quotations;
	}

	public void setQuotations(Set<Quotation> quotations) {
		this.quotations = quotations;
	}

	public Set<Category> getCategories() {
		return categories;
	}

	public void setCategories(Set<Category> categories) {
		this.categories = categories;
	}

	public Set<Product> getProducts() {
		return products;
	}

	public void setProducts(Set<Product> products) {
		this.products = products;
	}

	public Category getCategoryByID(long id) {

		Category category = null;
		for (Category iter : getCategories()) {
			if (iter.getId() == id) {
				category = iter;
				break;
			}
		}
		return category;
	}

	public Product getProductByID(long id) {

		Product product = null;
		for (Product iter : getProducts()) {
			if (iter.getId() == id) {
				product = iter;
				break;
			}
		}
		return product;
	}

	public Site getSiteByID(long id) {

		Site site = null;
		for (Site iter : getSites()) {
			if (iter.getId() == id) {
				site = iter;
				break;
			}
		}
		return site;
	}

	public Quotation getQuotationByID(long id) {

		Quotation quotation = null;
		for (Quotation iter : getQuotations()) {
			if (iter.getId() == id) {
				quotation = iter;
				break;
			}
		}
		return quotation;
	}

	public boolean addCategory(Category cat) {
		return categories.add(cat);
	}

	public boolean addSite(Site site) {
		return sites.add(site);
	}

	public boolean addProduct(Product product) {
		return products.add(product);
	}

	public boolean addQuotation(Quotation quotation) {
		return quotations.add(quotation);
	}

	public boolean removeCategory(Category cat) {
		return categories.remove(cat);
	}

	public boolean removeProduct(Product product) {
		return products.remove(product);
	}

	public boolean removeSite(Site site) {
		return sites.remove(site);
	}

	public boolean removeQuotation(Quotation quotation) {
		return quotations.remove(quotation);
	}

	public Quotation retrieveMinQuotationProduct(Long id) {

		float min = Float.MAX_VALUE;
		Quotation quotationMin = null;

		for (Quotation iter : getQuotationsByProduct(id)) {

			if (iter.getPrice() < min) {
				min = iter.getPrice();
				quotationMin = iter;
			}
		}
		return quotationMin;
	}

	public List<Quotation> getQuotationsByProduct(long id) {

		List<Quotation> list = new ArrayList<Quotation>();

		for (Quotation iter : getQuotations()) {
			if (iter.getProduct().getId() == id) {
				list.add(iter);
			}
		}
		return list;
	}

	public List<Quotation> retrieveProductsMinQuotation() {

		List<Quotation> minQuotations = new ArrayList<Quotation>();

		for (Product product : getProducts()) {
			Quotation minQuotation = retrieveMinQuotationProduct(product.getId());
			
			if	(minQuotation != null ){
				minQuotations.add(minQuotation);
			}
			
		}
		return minQuotations;
	}
}