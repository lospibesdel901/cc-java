package bd2.bd2.model;

import bd2.bd2.dto.CategoryDTO;

public class Category {
	
	
	public Category() {
		
	}
	
	public Category(CategoryDTO categoryDTO	) {
		name = categoryDTO.getName();
	}
	public void update(CategoryDTO categoryDTO) {
		name = categoryDTO.getName();
	}
	
	private Long id;
	private String name;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
